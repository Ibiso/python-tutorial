name = input("Whats your name? ")

if len(name) < 3:
    print("name must be at least 3 characters long")
elif len(name) > 50:
    print("name must be a max of 50 characters")
else:
    print("name looks good")
