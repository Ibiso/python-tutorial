command = ""
started = False
while True:
    command = input("> ").lower()
    if command == "start":
        if started:
            print("car already started!")
        else:
            started = True
            print("Car started...")
    elif command == "stop":
        if not started:
            print("car already stopped!")
        else:
            started = False
            print("Car Stoped...")
    elif command == "help":
        print('''
        start - To start a car
        stop -  To Stop the car
        quit - To quit
        ''')
    elif command == "quit":
        break
    else:
        print("Sorry! I don't understand that")